#define MAXSIZE 64
#define MAXARR 1024

struct person {
    char name[MAXSIZE], lastname[MAXSIZE];   
    int  id, salary;
};

int open_file(char *fname){
    if(fopen(fname, "r") == NULL){
		return -1;
	}
        
    return 0;
}

int read_int(int *address){
    if(scanf("%d", address) == 0){
		return -1;
	}   
    return 0;
}

int read_float(float *address){
    if(scanf("%f", address) == 0){
		return -1;
	}  
    return 0;
}

int read_string(char *address){
    if(scanf("%s", address) == 0){
		return -1;
	}
        
    return 0;
}

void close_file(FILE *fname){
	fclose(fname);
}
/*
compare specified variables,
it would be used in sort a list.
*/
int comp(const void *p1,const void *p2){
	int id1=((struct person *)p1)->id;
	int id2=((struct person *)p2)->id;
	return id1>id2?1:-1;
}

int binary_search(const int arr[],int min,int max,int target){
	if(max>=min){
		int mid=min+(max-min)/2;
		if(arr[mid]==target){
			return mid;
		}else if(arr[mid]>target){
			return binary_search(arr,min,mid-1,target);
		}else{
			return binary_search(arr,mid+1,max,target);
		}
	}
	return -1;
}

void print_by_key(struct person employee[MAXSIZE],int key){
	printf("*********************************************************\n");
	printf(" Name                     SALARY       ID\n");
	printf("""%-10s %-10s %10d %10d \n",employee[key].name,employee[key].lastname,employee[key].salary,employee[key].id);
	printf("*********************************************************\n");
}

int search_lastname(struct person employee[MAXSIZE],int emp_num,char lastname[]){
	int i=0;
	while(i!=emp_num){
		if(strcmp(employee[i].lastname,lastname)==0){
			return i;
		}
		i++;
	}
	return -1;
}
