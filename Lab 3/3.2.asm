	.data 0x10010000
var1:	.word 33	#var1 is a word with the initial value 33
var2:	.word 26	#var1 is a word with the initial value 26
var3:	.word  -2020

	.text 

main:	addu $s0,$ra,$0		#$0 save $31 in $16

	lw $t1,var1
	lw $t2,var2
	lw $t3,var3

	slt  $t0,$t2,$t1
        bgtz $t0,else
	sw  $t1,var2
        sw  $t2,var1
        beq $0,$0, Exit
else:   sw $t3,var1
	sw $t3,var2

Exit:   addu $ra,$0,$s0
	jr  $ra
