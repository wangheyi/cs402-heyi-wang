#Display environment
A program can be used in Windows, Unix, Linux system.


# Feature
	1.read data from a file and store data into the program.
	2.calculate the mean of data in the program and print the result.
	3.calcualte the standard deviation of data in the program and print the result.
	4.find the median of data in the program and print it.
                5.it would print the how many data in the program,and how much space unused.

It can work for 10 data values or for 10 million without re-compilation,use malloc()function to allocate space for data. If the data are large and the current space cannot accommodate the data, this program would allocate more space and creates an new array for the data automatically. The data in the current array would be copy into the new array. After that, the space of the current array would be free.

The program adopts some pointers instead of defining global varabiles, it can change the values of struct, when the proccess invokes functions.


#Introduction
  ** structure
This program uses top-down design.

Define some function and declare them before we use it in a main program.

  **function
	FILE *open_file(char *filename) //open a file outside, and judge whether it read successfully.
	double get_mean(float *p,int num) // calculation the mean of data in the program ,and return the mean value.
	double get_std(float *p,int num)  // calculation the standard deviation of data in the program ,and return the standard deviation value.
	double get_mid(float *arr,int num)// find the median values of data in the program and return the median value.
	void swap(float *p1,float *p2) // swap the values between 2 different addresses that pointers point to.
	float bubble_sort(float *arr,int num) 	//using bubblesort to sort the data in the program from smallest to largest.
	int main(int argc,char *argv[]) //excutable function

# How to use a program?

There're several steps could help you to run the program.

Step1: Put readfile.exe and a data file.

Step2: Open a terminal,and find a pragram path.
	command format:D: cd homework\path\...
                        display:D: \homework\path\...
      	sample: D: cd Homework\CS402\SP LAB-1>
	        display :D:\Homework\CS402\SP LAB-3>

Step3: In this step,you have two ways to execute a program.
         First(for windows):   enter a command in cmd,then executing a program.
       		command format: (program file name).exe (data file).txt
       		sample: basicstats.exe input.txt
         Second(for Unix and Linux):  command format: gcc -o (executable name) (file).c
		sample: gcc -o basicstats basicstats.c
	        command format: (executable name) (data file).txt
		sample:./basicstats small.txt

Step4: When the process exectues, the mean value, the standard deviation value, and the median value that of the data that input from a file outside would be printed on screen.

