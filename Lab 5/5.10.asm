	.data 0x10000000
	.align 0
ch1:	.byte  	'a'
word1:  	.word	0x89abcdef
ch2:	.byte 	'b'
word2:	.word	0

	.text
	.globl main

main:	lui $t1,0x1000
	ori $a0,$t1,0x1
	lwr $t0,0($a0)
	lwl $t0,3($a0)
	lui $t1,0x1000
	ori $a0,$t1,0x6
	swr $t0,0($a0)
	swl $t0,3($a0)
	jr $ra
