		.data 0x10010000
var1:       	.word 83              # var1 is a word (32 bit) with the ..
					# initial value 83
var2:		.word 104	      	# var2 is a word (32 bit) with the ..
					# initial value 104
var3:		.word 111	      	# var3 is a word (32 bit) with the ..
					# initial value 111
var4:		.word 119	      	# var4 is a word (32 bit) with the ..

					# initial value 119
first:		.byte 'H'	      	# first is a word (32 bit) with the ..
					# initial value H
last:		.byte 'W'	      	# last is a word (32 bit) with the ..
					# initial value W
		.text
		.globl main

main:      		lui $1, 4097	      	#load var1 value into register $t1
		lw $9, 0($1)             
		lui $1, 4097	      	#load var2 value into register $t2
		lw $10, 4($1)            
               		lui $1, 4097	      	#load var3 value into register $t3
                                lw $11, 8($1)            
		lui $1, 4097	      	#load var4 value into register $t4
		lw $12, 12($1)           
		lui $1, 4097		#load first value into register $t5
		lb $13, 16($1)
		lui $1, 4097		#load last value into register $t6
		lb $14, 17($1)
               		lui $1, 4097		#load first address into $t7
		ori $15, $1, 16
		lui $1, 4097		#load first address into $t0
		ori $8, $1, 17            
		lui $1, 4097	      	#store value of register $t1 into var4
		sw $9, 12($1)            
		lui $1, 4097		#store value of register $t2 into var3
		sw $10, 8($1)            
		lui $1, 4097		#store value of register $t3 into var2
		sw $11, 4($1)
		lui $1, 4097		#store value of register $t4 into var1	
		sw $12, 0($1)  
		ori $2,$0,10		#exit
		syscall          	


