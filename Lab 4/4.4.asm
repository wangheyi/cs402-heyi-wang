		.data
msg1:		.asciiz "the largest integer number: "	
		.text
		.globl main
main: 		addi $sp,$sp,-12	# first update the stack pointer
		sw  $ra,0($sp)  # push $ra into the stack

	
		li $v0,5		#read integer number	
		syscall
		sw $v0,4($sp)
		li $v0,5
		syscall
		sw $v0,8($sp)
		jal Largest
		lw $ra,0($sp)
		addi $sp,$sp,12  # shrink the stack by one word
		jr $ra 		# return from main


Largest:	li $v0,4
		la $a0, msg1
		syscall
		lw $a0,4($sp)	#pass parameter in $a0
		lw $a1,8($sp)
		slt $t2,$a1,$a0 #$t2=1,if$a1<$a0
		bne $t2,$0,L1	#if$a1<$a0,jump to L1
		move $a0,$a1
		

L1:		li $v0,1	#return the largest number and print it.
		syscall
		jr $ra

		
