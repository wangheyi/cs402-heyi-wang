		.data
msg1:		.asciiz "Please enter an integer number: "
msg2:		.asciiz "error.please eter again."	


		.text
		.globl main
main: 		addi  $sp,$sp,-4
		sw $ra, 0($sp) 		# must save $ra since I��ll have a call		
L2:		li $v0,4
		la $a0,msg1
		syscall
		li $v0,5
		syscall
		addu $a0,$v0,$0
		bltz $v0,L1
		jal Factorial
		addu $a0,$0,$v0
		li $v0,1
		syscall
		la $ra,0($sp)
		addi $sp,$sp,4
		jr $ra


L1:		li $v0,4
		la $a0,msg2
		syscall
		j L2


Factorial:	subu $sp, $sp, 4
		sw $ra, 4($sp) 		# save the return address on stack
		beqz $a0, terminate 	# test for termination
		subu $sp, $sp, 4 	# do not terminate yet
		sw $a0, 4($sp) 		# save the parameter
		sub $a0, $a0, 1 	# will call with a smaller argument
		jal Factorial
		lw $t0, 4($sp) 		# the argument I have saved on stack
		mul $v0, $v0, $t0 	# do the multiplication
		lw $ra, 8($sp) 		# prepare to return
		addu $sp, $sp, 8 	# I��ve popped 2 words (an address and
		jr $ra 			# .. an argument)
		

terminate:
		li $v0, 1 		# 0! = 1 is the return value
		lw $ra, 4($sp) 		# get the return address
		addu $sp, $sp, 4 	# adjust the stack pointer
		jr $ra 			# return
