		.text
		.globl main
main: 		move $s0, $ra 	# must save $ra since I’ll have a ca
		jal test 	# call ‘test’ with no parameter
		nop 		# execute this after ‘test’ returns
		move $ra, $s0 	# restore the return address in $ra
		jr $ra 		# return from main
test: 		nop 		# this is the procedure named ‘test’
		jr $ra 		# return from this procedure
