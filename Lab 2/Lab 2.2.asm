		.data 0x10010000
var1:       	.word 83              # var1 is a word (32 bit) with the ..
					# initial value 83
var2:		.word 104	      	# var2 is a word (32 bit) with the ..
					# initial value 104
var3:		.word 111	      	# var3 is a word (32 bit) with the ..
					# initial value 111
var4:		.word 119	      	# var4 is a word (32 bit) with the ..

					# initial value 119
first:		.byte 'H'	      	# first is a word (32 bit) with the ..
					# initial value H
last:		.byte 'W'	      	# last is a word (32 bit) with the ..
					# initial value W
		.text
		.globl main

main:      	addu $s0, $ra, $0	# save $31 in $16
		lw $t1,var1	      	#load var1 value into register $t1
		lw $t2,var2	      	#load var2 value into register $t2
               		lw $t3,var3	      	#load var3 value into register $t3
		lw $t4,var4	      	#load var4 value into register $t4
		lb $t5,first		#load first value into register $t5
		lb $t6,last		#load last value into register $t6
               		la $t7,first		#load first address into $t7
		la $t0,last		#load first address into $t0
		sw $t1,var4	      	#store value of register $t1 into var4
		sw $t2,var3		#store value of register $t2 into var3
		sw $t3,var2		#store value of register $t3 into var2
		sw $t4,var1		#store value of register $t4 into var1		
# restore now the return address in $ra and return from main
		addu $ra, $0, $s0	# return address back in $31
		jr $ra			# return from main


