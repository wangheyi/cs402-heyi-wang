		.data
msg1:		.asciiz "Please enter integer number: "
msg2:		.asciiz "error.please enter again."
msg3:		.asciiz "the result is: "	


		.text
		.globl main
main: 		li $v0,4
		la $a0,msg1
		syscall
		li $v0,5
		syscall
		move $t0,$v0
		li $v0,4
		la $a0,msg1
		syscall
		li $v0,5
		syscall
		move $t1,$v0
		bltz $t0,L1
		bltz $t1,L1
		li $v0,4
		la $a0,msg3
		syscall
		addi $sp,$sp,-4
		sw $ra,4($sp)
		move $a0,$t0
		move $a1,$t1
		jal Ackermann
		move $a0, $v0
    		li $v0, 1
    		syscall
    		lw $ra, 4($sp)
    		addi $sp, $sp, 4
    		jr $ra
		

L1:		li $v0,4
		la $a0,msg2
		syscall
		j main

Ackermann:	beq $a0,$0,X
		addi $sp,$sp,-4
		sw $ra, 4($sp)
		beq $a1,$0,Y
		addi $t0,$a0,-1
		addi $sp, $sp, -4
		sw $t0, 4($sp)
		addi $a1,$a1,-1
		jal Ackermann
		lw $a0, 4($sp)
    		addi $sp, $sp, 4
   		move $a1, $v0
    		jal Ackermann
		j end

X:		addi $v0,$a1,1
		jr $ra

Y:		addi $a0,$a0,-1
		li $a1,1
		jal Ackermann

end:		lw $ra, 4($sp)
    		addi $sp, $sp, 4
		jr $ra

