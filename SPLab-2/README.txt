#Display environment
A program can be used in Windows, Unix, Linux system.

# Feature
it can be used at a follow list:
	1.sort data in specified number.
	2.print all data
	3.search data
	4.add information in a data list,
	5.remove informations from a list.
	6.update informaiton.
	7.print a specified data on a list.
	8.search all records that in specified information on a list and print them.

No.5,6,7,8 are new feature that added in this program.

#Introduction
  ** structure
This program uses top-down design.

Define a struct, when the program read an external datafile, it need to follow the struct we define.

  **function
     ##in head file
         int open_file(char *fname)	 // Determines if the file was read successfully
         int read_int(int *address)	 	// enter address that type of integer into a program
         int read_float(float *address)  	// enter address that type of float into a program
         read_string(char *address)	//enter address that type of char
         close_file(FILE *fname)		//close to read an external file.
         int comp(const void *p1,const void *p2)  	//compare two different ID that in structs respectively.
         int comp_sal(const void *p1,const void *p2) //compare two different salary that in structs respectively.
         int binary_search(const int arr[],int min,int max,int target) //search data in binary way.
         void print_by_key(struct person employee[MAXSIZE],int key)// a printf format.
         int search_lastname(struct person employee[MAXSIZE],int emp_num,char lastname[]) // search a record that its lastname in list.
         int search_tlname(struct person employee[MAXSIZE],int emp_num,char lastname[])   //search all records that their lastname in list.

    ##in c file
        1.determine whether users enter a data file on command line.
        2.read data into a program
        3.build an menu in a screen,and each option has its own function. 

# How to use a program?
There're several steps could help you to run the program.

Step1: Put readfile.exe and input.txt(a data file needed to input in application) into a same folder.

Step2: Open a terminal,and find a pragram path.
	command format:D: cd homework\path\...
                        display:D: \homework\path\...
      	sample: D: cd Homework\CS402\SP LAB-1>
	        display :D:\Homework\CS402\SP LAB-1>

Step3:In this step,you have two ways to execute a program.
         First(for windows):   enter a command in cmd,then executing a program.
       		command format: (program file name).exe (data file).txt
       		sample: readfile.exe input.txt
         Second(for Unix and Linux):  command format: gcc -o (executable name) (file).c
		sample: gcc -o read readfile.c
	        command format: (executable name) (data file).txt
		sample: read input.txt

Step4: In a program, it will has has a menu, you can use this program by folloing some prompts.

