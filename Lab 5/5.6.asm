		.data
user1:		.word 0		
msg1:		.asciiz	 "please enter an integer: "
msg2:		.asciiz	 "If bytes were layed in reverse order the number would be:"

		.text
		.globl main
main: 		addi $sp,$sp,-4		# first update the stack pointer
		sw  $ra,0($sp)  	# push $ra into the stack
		la $a1,user1
		li $v0,4
		la $a0,msg1
		syscall
		li $v0,5
		syscall
		sw $v0,0($a1)
		jal Reverse_bytes 	# call ‘Reverse_bytes’ with no parameter
		li $v0,4
		la $a0,msg2
		syscall
		li $v0,1
		lw $a0,0($a1)
		syscall
               		 lw $ra,0($sp)   	# restore the return address
		addi $sp,$sp,4  	# shrink the stack by one word
		jr $ra 			# return from main



Reverse_bytes: 	 lb $t0,0($a1)
		lb $t1,1($a1)
		lb $t2,2($a1)
		lb $t3,3($a1)
		sb $t0,3($a1)
		sb $t1,2($a1)
		sb $t2,1($a1)
		sb $t3,0($a1)
		jr $ra
