	.data 0x10010000
var1:	.word 33	#var1 is a word with the initial value 33


	.text 

main:	addu $s0,$ra,$0		#$0 save $31 in $16
	lw $t0,var1
	lw $t1,var1
	li $a1,100
	

Loop:	ble $a1,$t0,Exit	
	addi $t1,$t1,1
	addi $t0,$t0,1
	j Loop

Exit:	sw $t1,var1
	addu $ra,$0,$s0
	jr  $ra
        
