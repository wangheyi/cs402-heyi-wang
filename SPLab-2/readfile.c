#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "readfile.h"

int main(int argc,char *argv[]){
	if(argc <2){
		printf("pass filename to read\n");
		return 0;
	}// determine whether user enter a data file in command line
	/*
	defined some variables ,arrary and pointer that would be used in program.
	read a file into a program with a structure it defined.
	*/
	char *fname=argv[1],name[MAXSIZE],lastname[MAXSIZE];
	struct person employee[MAXSIZE];
	int option,six_digit_ID,key,salary,confirm;

	int emp_num=0,list[MAXARR],list_sa[MAXARR];
	if (open_file(fname)==-1){
		printf("error reading file\n");
		return -1;
	}
	FILE *fp=fopen(fname,"r");
	while(!feof(fp)){
		fscanf(fp,"%d  %s  %s  %d\n",&employee[emp_num].id,&employee[emp_num].name,&employee[emp_num].lastname,&employee[emp_num].salary);
		list[emp_num]=employee[emp_num].id;
		list_sa[emp_num]=employee[emp_num].salary;
		emp_num++;
	}
	fclose(fp);
	/*
		build a menu
		supply some choice ,each choice has its own function
		loop the menu, unless it choice quit function in menu.
	*/
	while(option!=5){
		printf("\nEmployee DB Menu:\n");
		printf("********************************************************\n");
		printf("(1) Print the Database\n");
		printf("(2) Lookup by ID\n");
		printf("(3) Lookup by Last Name\n");
		printf("(4) Add an Employee\n");
		printf("(5) Quit\n");
		printf("(6) Remove an employee\n");
		printf("(7) Update an employee's information\n");
		printf("(8) Print the M employees with the highest salaries\n");
		printf("(9) Find all employees with matching last name\n");
		printf("**********************\n");
		printf("Enter your choice: ");
		read_int(&option);
		qsort(employee,emp_num,sizeof(employee[0]),comp);
			for(int i=0;i<emp_num;i++){
		 	list[i]=employee[i].id;
			}
		
		switch(option){
		case 1:
			printf("********************************************************\n");
			printf(" Name                     SALARY       ID\n");
			printf("********************************************************\n");
			int i=0;
			while(i!=emp_num){
				printf("%-10s %-10s %10d %10d \n",employee[i].name,employee[i].lastname,employee[i].salary,employee[i].id);
				i++;
			}
			printf("*********************************************************\n");
			printf("Number of Employees (%d)\n",emp_num);
			break;
		case 2:
			printf("Enter a 6 digit employee id: ");
			read_int(&six_digit_ID);
			key =binary_search(list,0,emp_num,six_digit_ID);
			if(key==-1){
				printf("Employee with id %d not found in DB\n",six_digit_ID);
			}else{
				print_by_key(employee,key);
			}
			break;
			

		case 3:
			printf("Enter Employee's last name (no extra spaces): ");
			read_string(lastname);
			key = search_lastname(employee,emp_num,lastname);
			if(key==-1){
				printf("Employee with lastname %s not found in DB\n",lastname);
			}else{
				print_by_key(employee,key);
			}
			break;
		case 4:
			printf("Enter the first name of the employee: ");
			read_string(name);
			printf("Enter the last name of the employee: ");
			read_string(lastname);
			printf("Enter employee's salary ($30,000 and $150,000): ");
			read_int(&salary);
			printf("do you want to add the following employee to the DB?\n");
			printf("%s %s , salary: %d\n",name,lastname,salary);
			printf("Enter 1 for yes, 0 for no: ");
			read_int(&confirm);
			if(confirm==1){
				if(salary>=30000&&salary<=150000){
					int cur_id=employee[emp_num-1].id;
					int new_id=cur_id+1;
					if(new_id<100000||new_id>999999){
						printf("id number is out of bound\n");
						break;
					}
					strcpy(employee[emp_num].name,name);
					strcpy(employee[emp_num].lastname,lastname);
					employee[emp_num].salary=salary;
					employee[emp_num].id=new_id;
					list[emp_num]=employee[emp_num].id;
					emp_num++;
					printf("adding successfully.\n");
				}else{
					printf("salary invalid, please enter again.\n");
				}
			}
			break;

		case 5:
			printf("goodbye!\n");
			break;
		case 6:
		    printf("Enter Employee's ID: ");
			read_int(&six_digit_ID);
			int key=0;
			key = binary_search(list,0,emp_num,six_digit_ID);
			if(key==-1){
				printf("Employee %s %s not found in DB\n",name,lastname);
			}else{
				for(int i=key;key<emp_num;key++){
					employee[key]=employee[key+1];
				}
				emp_num=emp_num-1;
				printf("delete successfully.\n");
			}
			break;
	
		case 7:
	        printf("Enter Employee's ID: ");
			read_int(&six_digit_ID);
			key = binary_search(list,0,emp_num,six_digit_ID);
			if(key==-1){
				printf("Employee's ID %d not found in DB\n",six_digit_ID);
			}else{
				print_by_key(employee,key);
					printf("enter a new ID:");
					read_int(&six_digit_ID);
					while(six_digit_ID<100000||six_digit_ID>999999){
						printf("ID number out of bound,please try again!\n");
						printf("enter a new ID:");
						read_int(&six_digit_ID);
					}
					int key1=binary_search(list,0,emp_num,six_digit_ID);
					while(key1!=-1&&key1<=emp_num)
					{
						printf("ID number is exist,please try again!\n");
						printf("enter a new ID:");
						read_int(&six_digit_ID);
						key1=binary_search(list,0,emp_num,six_digit_ID);
					}
					printf("enter fisrtname:");
					read_string(name);
					printf("enter lastname:");
					read_string(lastname);
					int key2=search_name(employee,emp_num,name,lastname);
					while(strcmp(employee[key2].name,name)==0&&strcmp(employee[key2].lastname,lastname)==0){
						printf("this name is exist,please try again!\n");
						printf("enter fisrtname:");
						read_string(name);
						printf("enter lastname:");
						read_string(lastname);
						key2=search_name(employee,emp_num,name,lastname);
					}
					printf("enter a new salary:");
					read_int(&salary);
					while (salary<30000||salary>150000)
					{
						printf("salary out of bound,please try again!\n");
						printf("enter a new salary:");
						read_int(&salary);
					}
					printf("do you want to update the informations to the DB?\n");
					printf("%s %s , salary: %d,ID: %d\n",name,lastname,salary,six_digit_ID);
					printf("Enter 1 for yes, 0 for no: ");
					read_int(&confirm);
					if(confirm==1){
						employee[key].id=six_digit_ID;
						strcpy(employee[key].name,name);
						strcpy(employee[key].lastname,lastname);
						employee[key].salary=salary;
						printf("update successfully\n");
						printf("*********************************************************\n");
						print_by_key(employee,key);
					}else{
						printf("there's nothing to update.\n");
					}
			}	
			break;
		case 8:
		printf("enter number of printing:");
		int m=0;
		read_int(&m);
		while(m<0||m>emp_num){
			printf("m is out of bound.\n");
			printf("enter number of printing:");
			read_int(&m);
		}
		struct person *copy=employee;
		qsort(copy,emp_num,sizeof(copy[0]),comp_sal);
		printf("*********************************************************\n");
		printf(" Name                     SALARY       ID\n");
		int o=0;
		while(o<m){
			printf("%-10s %-10s %10d %10d \n",copy[emp_num-1-o].name,copy[emp_num-1-o].lastname,copy[emp_num-1-o].salary,copy[emp_num-1-o].id);
			o++;
		}
		printf("*********************************************************\n");
		break;
		case 9:
		printf("enter a lastname:");
		read_string(lastname);
		search_tlname(employee,emp_num,lastname);
		key = search_lastname(employee,emp_num,lastname);
			if(key==-1){
				printf("Employee with lastname %s not found in DB\n",lastname);
			}
		break;
		default:
			printf("Hey, %d is not between 1 to 5, please make your choice again.\n",option);
			break;
		}
	}
}
