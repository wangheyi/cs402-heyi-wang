		.text
		.globl main
main: 		addi $sp,$sp,-4	# first update the stack pointer
		sw  $ra,0($sp)  # push $ra into the stack
		jal test 	# call ‘test’ with no parameter
		nop 		# execute this after ‘test’ returns
                lw $ra,0($sp)   # restore the return address
		addi $sp,$sp,4  # shrink the stack by one word
		jr $ra 		# return from main
test: 		nop 		# this is the procedure named ‘test’
		jr $ra 		# return from this procedure
