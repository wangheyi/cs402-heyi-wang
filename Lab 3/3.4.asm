		.data 0x10010000
my_array:	.space 40	 # allocated 40 bytes of space
initial_value:  .word 33

		.text 

main:	addu $s0,$ra,$0		#$0 save $31 in $16
	lw $t1,initial_value
	la $t0,my_array
	addi $t7,$t0,40

Loop:	ble $t7,$t0,Exit	
	sw $t1,0($t0)
	addi $t1,$t1,1
	addi $t0,$t0,4
	j Loop

Exit:	addu $ra,$0,$s0
	jr  $ra
        
