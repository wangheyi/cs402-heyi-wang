	.data 0x10000000
msg1:	.asciiz "I'm far away "
msg2:	.asciiz "I'm nearby "
far:    .word 1
	.text

main:   addu $s0, $ra, $0 # save $31 in $16
	li $v0, 5 # system call for read_int
	syscall # the integer placed in $v0

	addu $t0, $v0, $0 # move the number in $t0

	li $v0, 5 # system call for read_int
	syscall # the integer placed in $v0
	addu $t1,$v0,$0
	bne $t0,$t1,near
        li $v0,4
	la $a0,msg1
	syscall
        b far

near:	li $v0,4
	la $a0,msg2
	syscall
	jr $ra



