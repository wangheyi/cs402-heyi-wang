package hw_1;

import java.util.Random;

public class Matrix {
	public static int[][] Matrix;
	  private int length1;
	  private int length2;
	  private int length3;
	 
	  static Random ran = new Random();
	  
	  public double getlength1(){
			return length1;
		}
	  public double getlength2(){
			return length2;
		}
	  public double getlength3(){
			return length3;
		}
	  public void setlength1(int l1){
			this.length1=l1;
		}
	  public void setlength2(int l2){
			this.length2=l2;
		}
	  public void setlength3(int l3){
			this.length3=l3;
		}
	  public static void getAB(int[][]a,int[][]b){
		  for(int i=0;i<a.length;i++){
			  for(int j=0;j<a[0].length;j++){
				  a[i][j]=ran.nextInt(100);
		   }
		  }
		  for(int i=0;i<b.length;i++){
			  for(int j=0;j<b[0].length;j++){
				  b[i][j]=ran.nextInt(100);
			   }
		  }
		  System.out.println("print MatrixA");
		  for(int i=0;i<a.length;i++){
			  for(int j=0;j<a[0].length;j++){
				  System.out.print(a[i][j]+" ");
				  }
			  System.out.println("");
			  }
		  System.out.println("print MatrixB");
		  for(int i=0;i<b.length;i++){
			  for(int j=0;j<b[0].length;j++){
				  System.out.print(b[i][j]+" ");
				  }
			  System.out.println("");
			  }
		  }
	  public static void mMatrix(int[][] a,int[][] b){
	      System.out.println("print MatrixA multiple MatrixB");
	      Matrix=new int[a.length][b[0].length];
	      for (int i = 0; i<a.length; i++) {
	      for (int j = 0; j<b[0].length; j++) {
	      for (int k = 0; k<a[0].length; k++) {
	    	  Matrix[i][j]=Matrix[i][j]+a[i][k]*b[k][j];//formula
	      }
	  }
	 }
	 }
	 public static void main(String[] args) {
		  
		 int l1= 3;
		 int l2=4;
		 int l3=5;

		  int [][]a = new int[l2][l1];
		  int [][]b = new int[l3][l1];
		  getAB(a,b);
		  mMatrix(a,b);
		  for (int i = 0; i<Matrix.length; i++) {
			   for (int j = 0; j<Matrix[0].length; j++) {
			   System.out.print (Matrix[i][j]+" ");
			   }
			   System.out.println ("");
			   }
		  String str="";
		  long starTime=System.currentTimeMillis();
		  for(int i=0;i<10000;i++){
		   str=str+i;
		  }
		  long endTime=System.currentTimeMillis();
		  long Time=endTime-starTime;
		  System.out.println("time is: ");
		  System.out.println(Time);
		  
		 }

}
