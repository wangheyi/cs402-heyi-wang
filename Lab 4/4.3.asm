		.data
msg1:		.asciiz "the largest integer number: "	
		.text
		.globl main
main: 		addi $sp,$sp,-4		# first update the stack pointer
		sw  $ra,0($sp)  	# push $ra into the stack


		li $v0,5		#read integer number	
		syscall
		add $t0,$t0,$v0		#stores a number into $t0
		li $v0,5
		syscall
		add $t1,$t1,$v0
		jal Largest
		lw $ra,0($sp)		#restore $ra
		addi $sp,$sp,4  	# shrink the stack by one word
		jr $ra 			# return from main


Largest:	li $v0,4
		la $a0, msg1
		syscall
		move $a0,$t0		#pass parameter in $a0
		move $a1,$t1
		slt $t2,$a1,$a0 	#$t2=1,if$a1<$a0
		bne $t2,$0,L1		#if$a1<$a0,jump to L1
		move $a0,$a1
		

L1:		li $v0,1		#return the largest number and print it.
		syscall
		jr $ra

		
