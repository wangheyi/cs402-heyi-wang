#include<stdio.h>
#include<stdlib.h>
int main()
{
    int X,Y;
    int Ackermann(int X,int Y);
    
    printf("Enter X:");
    scanf("%d",&X);
    printf("Enter Y:");
    scanf("%d",&Y);
    
    printf("Ackermann(%d,%d)=%d",X,Y,Ackermann(X,Y));
    
    return 0;
}
int Ackermann(int X,int Y)
{
    if(X==0){    
        return Y+1;
    }
    
    else 
        if(Y==0){
            return Ackermann(X-1,1);
        }
        else{
            return Ackermann(X-1,Ackermann(X,Y-1));
        }    
}
