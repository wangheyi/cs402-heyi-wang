#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


/*
   open a file outside.
*/
FILE *open_file(char *filename){
    if(fopen(filename,"r")==NULL){
        printf("Error reading!\n");
        exit(0);
    }
}

//a function to get mean.
double get_mean(float *p,int num){
    double sum=0;
    for(int i=0;i<num;i++){
        sum+=p[i];
    }
    double mean=sum/num;
    return mean;
}

//a function to get standard deviation
double get_std(float *p,int num){
    double std=0;
    double var=0;
    double mean=get_mean(p,num);
    for(int i=0;i<num;i++){
        var+=pow(p[i]-mean,2);
    }
    std=sqrt(var/num);
    return std;
}

//a function to get median,
double get_mid(float *arr,int num){
    if(num%2==1){
        return(arr[num/2]);
    }else{
        double mid1=arr[num/2-1];
        double mid2=arr[num/2];
        double mid=(mid1+mid2)/2;
        return mid;
    }
}

// a function to sort array
void swap(float *p1,float *p2){
    float temp=*p1;
    *p1=*p2;
    *p2=temp;
}

float bubble_sort(float *arr,int num){
    for(int i=0;i<num-1;i++){
        for(int j=i+1;j<num;j++){
            if(arr[i]>arr[j]){
                swap(&arr[i],&arr[j]);
            }
        }
    }
}


int main(int argc,char *argv[]){
    if(argc<2){
        printf("no file to read.\n");
        return 0;
    }
    int num=20, arr_length=0;
    float temp;
    float *arr=(float *)malloc(num*sizeof(float));
    char *filename=argv[1];
    FILE *fp=open_file(filename);

    while(!feof(fp)){
        fscanf(fp,"%f\n",(arr+arr_length));
        arr_length++;
        if(arr_length==num){  
            //the current length is equal to max capacity, then create a new array to expand the space.
            float *new_arr=(float *)malloc(2*num*sizeof(float));
            //copy value from the old array to new one.
            memcpy(new_arr,arr,arr_length*sizeof(float));
            // free memory.
            free(arr);
            arr=new_arr;
            //increase the size.
            num=num*2;
        }
    }
    fclose(fp);
    double mean=get_mean(arr,arr_length);
    double std=get_std(arr,arr_length);
    bubble_sort(arr,arr_length);
    double mid=get_mid(arr,arr_length);
    printf("Num values: %d\n",arr_length);
    printf("The mean is: %.3f\n",mean);
    printf("The median is: %.3f\n",mid);
    printf("The standard deviation is: %.3f\n",std);
    printf("Unused array capacity: %d\n",num-arr_length);
}
