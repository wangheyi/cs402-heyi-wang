#Display environment
this program should be executed in windows system

# Feature
it can be used to search data, add information in a data list,and sort data in specified number.

#Introduction
  ** structure
this program uses top-down design.First define a struct, when the program read an external datafile, it need to follow the struct we define.

  **function
     ##in head file
         int open_file(char *fname)	 // Determines if the file was read successfully
         int read_int(int *address)	 	// enter address that type of integer into a program
         int read_float(float *address)  	// enter address that type of float into a program
         read_string(char *address)	//enter address that type of char
         close_file(FILE *fname)		//close to read an external file.
         int comp(const void *p1,const void *p2)  	//compare two specified variables that in structs respectively.
         int binary_search(const int arr[],int min,int max,int target) //search data in binary way.
         void print_by_key(struct person employee[MAXSIZE],int key)// a printf format.
         int search_lastname(struct person employee[MAXSIZE],int emp_num,char lastname[])// search lastname in list.

    ##in c file
        1.determine whether users enter a data file on command line.
        2.read data into a program
        3.build an menu in a screen,and each option has its own function. 

# How to use a program?
there're several steps could help you to run the program.

Step1: put readfile.exe and input.txt(a data file needed to input in application) into a same folder.

Step2: win+r open a operation, enter 'cmd' search, and open a cmd.exe.

Step3: use a few commands in cmd.exe and enter a folder which contains executable application.
       command format:D: cd homework\path\...
                        display:D: \homework\path\...
       sample: D: cd Homework\CS402\SP LAB-1>
	display :D:\Homework\CS402\SP LAB-1>
		    
        enter in the specified path.

Step4:in this step,you have two ways to execute a program.
         First:   enter a command in cmd,then executing a program.
       		command format: (program file name).exe (data file).txt
       		sample: readfile.exe input.txt
         Second:  command format: gcc -o (executable name) (file).c
		sample: gcc -o read readfile.c
	        command format: (executable name) (data file).txt
		sample: read input.txt
          Both two ways can execute a program, you can choose one of them.

Step5: in a program, it will has has a menu, you can use this program by folloing some prompts.

