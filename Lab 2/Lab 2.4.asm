		.data 0x10010000
var1:       		.word 27              	# var1 is a word (32 bit) with the initial value 27
					 
var2:		.word 36	      	# var2 is a word (32 bit) with the initial value 36
					 
		.extern ext1 4      	#ext1 is a word(32 bit) in the extern space
		.extern ext2 4	#ext1 is a word(32 bit) in the extern space
	
		
		.text
		.globl main

main:      	addu $s0, $ra, $0	# save $31 in $16
		lw $t1,var1	      	#load var1 value into register $t1
		lw $t2,var2	      	#load var1 value into register $t2
		sw $t1,ext1		#store value of register $t1 into ext1
		sw $t2,ext2		#store value of register $t2 into ext2
		la $t3,ext1		#load ext1 address into register $t3
		la $t4,ext2		#load ext2 address into register $t4
# restore now the return address in $ra and return from main
		addu $ra, $0, $s0	# return address back in $31
		jr $ra			# return from main


